
# BME280 EXAMPLE
This example demostrates how to use the BME280 sensor with arduino, and get the environment temperature, pressure and humidity with I2C communiction protocol to exchange data with a microcontroller

## How to use example
You will need the following hardware to make it work:

-BME280 sensor 

-ESP32 Wireless Stick

-Jumpers

## How to connect to ESP32

BME280 has four pins which belong to VCC, GND, SDA, SCL. So, connect VCC to respective VCC (3.3v) for your board. Likewise for GND, 
finally for SDA and SCL pins you must choose where would you like to test the sensor, for this case we'll use the pin 9 for SDA and pin 10 for SCL.

Then, when you uploaded your code and it's running you should see the following output

![Logo](https://i0.wp.com/randomnerdtutorials.com/wp-content/uploads/2019/06/bme280-readings-arduino-ide.png?w=666&quality=100&strip=all&ssl=1)

## Troubleshooting
If you have issues with output or don't see any kind of information, check wiring and baudrate selected for console
and remember if you use pins as wireless stick's schematic say you won't see output information.