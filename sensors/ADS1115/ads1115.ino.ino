#include <Adafruit_ADS1X15.h>

Adafruit_ADS1115 ads;  /* Use this for the 16-bit version */
//Adafruit_ADS1015 ads;     /* Use this for the 12-bit version */
//Adafruit_ADS1115 ads2; 
void setup(void)
{
  delay(5000);
  Serial.begin(115200);
  Serial.println("Hello!");

  Serial.println("Getting single-ended readings from AIN0..3");
  Serial.println("ADC Range: +/- 6.144V (1 bit = 3mV/ADS1015, 0.1875mV/ADS1115)");

  // The ADC input range (or gain) can be changed via the following
  // functions, but be careful never to exceed VDD +0.3V max, or to
  // exceed the upper and lower limits if you adjust the input range!
  // Setting these values incorrectly may destroy your ADC!
  //                                                                ADS1015  ADS1115
  //                                                                -------  -------
  ads.setGain(GAIN_TWOTHIRDS);  // 2/3x gain +/- 6.144V  1 bit = 3mV      0.1875mV (default)
  //ads2.setGain(GAIN_TWOTHIRDS);  // 2/3x gain +/- 6.144V  1 bit = 3mV      0.1875mV (default)

  // ads.setGain(GAIN_ONE);        // 1x gain   +/- 4.096V  1 bit = 2mV      0.125mV
  // ads.setGain(GAIN_TWO);        // 2x gain   +/- 2.048V  1 bit = 1mV      0.0625mV
  // ads.setGain(GAIN_FOUR);       // 4x gain   +/- 1.024V  1 bit = 0.5mV    0.03125mV
  // ads.setGain(GAIN_EIGHT);      // 8x gain   +/- 0.512V  1 bit = 0.25mV   0.015625mV
  // ads.setGain(GAIN_SIXTEEN);    // 16x gain  +/- 0.256V  1 bit = 0.125mV  0.0078125mV

//ADS Domus
  if (!ads.begin(0x48)) {
    Serial.println("Failed to initialize ADS.");
    while (1);
  }
//ADS Externo 
  /*if (!ads2.begin(0x48)) {
    Serial.println("Failed to initialize ADS2.");
    while (1);
  }*/
}

void loop(void)
{
  int16_t adc0, adc1, adc2, adc3;
  float volts0, volts1, volts2, volts3;
  /*int16_t adc4, adc5, adc6, adc7;
  float volts4, volts5, volts6, volts7;*/

  adc0 = ads.readADC_SingleEnded(0);
  adc1 = ads.readADC_SingleEnded(1);
  adc2 = ads.readADC_SingleEnded(2);
  adc3 = ads.readADC_SingleEnded(3);

  volts0 = ads.computeVolts(adc0);
  volts1 = ads.computeVolts(adc1);
  volts2 = ads.computeVolts(adc2);
  volts3 = ads.computeVolts(adc3);

  
  /*adc4 = ads2.readADC_SingleEnded(0);
  adc5 = ads2.readADC_SingleEnded(1);
  adc6 = ads2.readADC_SingleEnded(2);
  adc7 = ads2.readADC_SingleEnded(3);

  volts4 = ads2.computeVolts(adc4);
  volts5 = ads2.computeVolts(adc5);
  volts6 = ads2.computeVolts(adc6);
  volts7 = ads2.computeVolts(adc7);*/

  Serial.println("-----------------------------------------------------------");
  Serial.println("Mediciones de ADC Domus");
  Serial.print("AIN0: "); Serial.print(adc0); Serial.print("  "); Serial.print(volts0); Serial.println("V");
  Serial.print("AIN1: "); Serial.print(adc1); Serial.print("  "); Serial.print(volts1); Serial.println("V");
  Serial.print("AIN2: "); Serial.print(adc2); Serial.print("  "); Serial.print(volts2); Serial.println("V");
  Serial.print("AIN3: "); Serial.print(adc3); Serial.print("  "); Serial.print(volts3); Serial.println("V");

  
  /*Serial.println("-----------------------------------------------------------");
  Serial.println("Mediciones de ADC Externo");
  Serial.print("AIN0: "); Serial.print(adc4); Serial.print("  "); Serial.print(volts4); Serial.println("V");
  Serial.print("AIN1: "); Serial.print(adc5); Serial.print("  "); Serial.print(volts5); Serial.println("V");
  Serial.print("AIN2: "); Serial.print(adc6); Serial.print("  "); Serial.print(volts6); Serial.println("V");
  Serial.print("AIN3: "); Serial.print(adc7); Serial.print("  "); Serial.print(volts7); Serial.println("V");*/

  delay(1000);
}
