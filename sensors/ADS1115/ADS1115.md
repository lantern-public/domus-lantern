
# ADS1115 EXAMPLE
This example demostrates how to use the ADS1115 with arduino. 
The ADS1115 is a analog to digital converter and has four analog channels.
It can be used in to measure both positive and negative voltages.

## How to use example
You will need the following hardware to make it work:

-ADS1115 

-ESP32 Wireless Stick

-Jumpers

-Breadboard

-Battery (For measure it)

## How to connect to ESP32
First step is installing the library required for this project in arduino IDE 
"Adafruit ADS1X15 by Adafruit"

Then, we will use the next ADS1115 pins:

- VCC
- GND 
- SDA (GPIO 21 Wireless Stick)
- SCL (GPIO 22 Wireless Stick)
- ADDR (Connected to GND)
- A0 (gets measures, connect a vcc from battery)

After that, upload the code and open the Serial Monitor to see what is happening and check the measurements

## Output
After uploading the code, you should get your measurements readings displayed in the Serial Monitor:



```bash
Mediciones de ADC Domus
AIN0: 19296  3.62V
AIN1: 5072  0.95V
AIN2: 4592  0.86V
AIN3: 4848  0.91V
```

## Troubleshooting
If you have issues with output or don't see any kind of information, check wiring and baudrate selected for console.
Re-install the library for fix any kind of issues and check the address with I2C scanner.