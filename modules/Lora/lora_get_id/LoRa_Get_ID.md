# Lora ID example
This example is used to get the ID from esp32
## How to use
Upload de code, then check your ID, and save it, it will be use to integrate to Lora
## Example Output
You will see an output as the follow
```
ESP32ChipID=E47A6D60A124

```
## Troubleshooting
If you have issues check if the device is connected correctly.
