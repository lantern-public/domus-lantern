# LORAWAN OTAA
## How to use
In this example we will know how to use Lora OTAA with Arduino, using the Wireless Stick Lite board

For this example, open the file and change the keys by the yours from ESP32 Wireless Stick Lite

You have to write the belong keys provided by your end device registered in ttn,
for the DevEUI, remember you have to write in little endian if not, the project won't work as expected

## Example Output
After to flash the board, open the Serial Monitor to wait the moment Wireless is trying to connect to gateway and sending a message,
this could take around five or six minutes, you just have to wait

![Logo](https://media.discordapp.net/attachments/930133519055847525/1007380212113223731/Screenshot_from_2022-08-11_14-09-04.png?width=1172&height=660)

As you can see, is trying to connect and doing a joing, after that, you will see a different message
and you can see the uplink in the ttn console as this:
![Logo](https://cdn.discordapp.com/attachments/930133519055847525/1007372156004475011/unknown.png)
## Troubleshooting
Check if you have set the parameters correctly, for example the keys, only the DevEUI is written in Little endian
and check if the gateway is connected to internet as board is connected.
Take the MAC Payload and convert this to ASCI to see the message.