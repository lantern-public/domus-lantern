# HTTP POST TEXT EXAMPLE
This is an example to make a HTTP POST from ESP32 with Arduino sending information with text plain in Content-Type

## How to use 
First, we need to include the libraries 
```
#include <WiFi.h>
#incluide <HTTPClient.h>

```
Then, set up the variables SSID, PASSWORD, http.begin (endpoint for POST)
Initialize the wifi module, and http client, add the header for Content-Type
build the message and what's the status is showing by console.
In httprespondeCode we choose the message in that slot "Posting from ESP32"
## Example Output
```
Connecting to WiFi..
Connected to the WiFi network
201
{
  "id": 101
}

}
```
## Troubleshooting
Check the connection for wifi network, availability, and status, the correct order in structure
and the endpoint needs to be available too 

(For this example beeceptor doesn't works as expected)