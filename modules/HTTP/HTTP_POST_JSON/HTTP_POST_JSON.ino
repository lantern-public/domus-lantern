#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

const char* ssid = "electric";
const char* password =  "electric";
const char* server = "https://jsonplaceholder.typicode.com/posts";
char jsonOutput[128];
  
void setup() {
  
  Serial.begin(115200);
  delay(4000);   //Delay needed before calling the WiFi.begin
  
  WiFi.begin(ssid, password); 
  
  while (WiFi.status() != WL_CONNECTED) { //Check for the connection
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }
  
  Serial.println("Connected to the WiFi network");
  
}
  
void loop() {
  
 if(WiFi.status()== WL_CONNECTED){   //Check WiFi connection status
  
   HTTPClient client;   
  
   client.begin(server);  //Specify destination for HTTP request
   client.addHeader("Content-Type", "application/json"); //Specify content-type header
   const size_t CAPACITY = JSON_OBJECT_SIZE(1);
   StaticJsonDocument<CAPACITY> doc;
   /*
   StaticJsonDocument<200> doc;
    // Add values in the document
    //
   doc["name"] = "brandon";
   doc["hello"] = "from esp32";
   // Add an array.
    //
   JsonArray data = doc.createNestedArray("data");
   data.add(9.948320);
   data.add(-84.144109);
     
   String requestBody;
   serializeJson(doc, requestBody);
   */

   JsonObject object = doc.to<JsonObject>();
   object["tittle"] = "Hello from ESP32";
   serializeJson(doc, jsonOutput); 

   int httpCode = client.POST(String(jsonOutput));   //Send the actual POST request
  
   if(httpCode>0){
  
      String payload = client.getString();   
      //Get the response to the request
    
      Serial.println("\nStatuscode: " + String(httpCode));   //Print return code
      Serial.println(payload); //Print request answer
  
      client.end();

    }
  else {
  
     Serial.print("Error on sending POST: ");//This for know if the POST had a problem  
   }
 }
 else
 {
    Serial.println("Connection lost");
  }
  
  delay(10000);
}

  
