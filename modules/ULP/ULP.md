# ULP EXAMPLE
In this example we will know how to make ULP works and enable with RTC peripherals using Arduino
## How to use 
For this example, open the file and change times for deep sleep if necessary. With this code, disabled all the modules as Lora GPIOs,
 Wifi, Bluetooth, Radio, ESP32 Core and Memory to get the minimum consumption approximately with 10uA

![Logo](https://lastminuteengineers.b-cdn.net/wp-content/uploads/arduino/ESP32-Deep-Sleep-Functional-Block-Diagram.png)

For this example, we disabled the led wich ESP32 turn on when is in deep sleep, because we are getting 87uA, that's not the expected

```
digitalWrite(25,LOW);
```
## Example Output
After to flash the board, connect the otii to get consumption, needs to be close to 10 and 15uA
```
Serial initial done
The Wireless Stick Lite not have an on board display, Display option must be FALSE!!!
Boot number: 2
Wakeup caused by touchpad
Setup ESP32 to sleep for every 10 Seconds
Going to sleep now

```
## Troubleshooting
Check if you have set the parameters correctly, and LED not must be blinkin.
Wait 10 seconds to see the ESP32 turning on/off