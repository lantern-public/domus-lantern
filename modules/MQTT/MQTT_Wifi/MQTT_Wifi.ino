#include <WiFi.h>
#include <PubSubClient.h>

// Replace the next variables with your SSID/Password combination
const char* ssid = "E2E-LANTERN";
const char* password = "LantE8846*";

// Add your MQTT Broker IP address, example:
//const char* mqtt_server = "192.168.1.144";
const char* mqtt_server = "10.200.107.109"; //change the ip broker for your computer ip

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;
//initialize the client server and callback
void setup() {
  Serial.begin(115200);
  delay(10);
  setup_wifi();
  client.setServer(   mqtt_server, 1883);
  client.setCallback(callback);
}
 // We start by connecting to a WiFi network and print the device ip address
void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}
//Select the type of message and send
void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Message arrived on topic: ");
  Serial.print(topic);
  Serial.print(". Message: ");
  String messageTemp;
  
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();

}
//Trying to stablish a connection with MQTT, then print a message it's ready to susbcribe to topic "esp32/output"
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      // Subscribe
      client.subscribe("esp32/output");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
///When the message has been sent, check if client is disconnected and send message again
void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
    
  client.publish("esp32/input ","hello from esp32!"); //This is the topic you must use for subscribe eg. mosquitto_sub -h localhost -p 1883 -t esp32/input -v

  
}
