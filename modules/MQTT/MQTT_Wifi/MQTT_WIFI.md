# MQTT example
Here's a brief description about the MQTT example for ESP32 integrated with Arduino 
We will use a MQTT process for send a message to our computer used as broker that receives
messages from an specific topic.

## How to use
First, we have to connect the ESP32 to the same network your computer is connected, then, search 
in your computer your IP Address given by the network and write it in mqtt_server constant.
Initialize the wifi module and connect to the network, after that build the message with the topic would you like to subscribe for 
and try to stablish a connection between device and computer by MQTT and the belong topic chosed, you must to wait while the device 
is sending the information and you can subscribe with the topic previously said.
Write this command in a terminal tou watch the message "mosquitto_sub -h localhost -p 1883 -t esp32/input -v"
## Example Output
You will see an output as the follow
```
Connecting to E2E-LANTERN
..
WiFi connected
IP address: 
10.200.107.26
Attempting MQTT connection...connected

```
At that moment, it's ready to subscribe writing the given command, this must be the output:

```
esp32/input "Hello from esp32"
```
## Troubleshooting
If you have issues check if the network is turned on and available, verifying the topic you are subscribing and the baudrate for serial monitor
Check you have mosquitto in your computer, otherwise, install it and try again
