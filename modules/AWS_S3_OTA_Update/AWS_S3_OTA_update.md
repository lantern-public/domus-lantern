# OTA EXAMPLE AWS
In this example we will know how to make OTA for ESP32 from the S3 (AWS) bucket using arduino
## How to use 
First, we need to open the example given from Arduino in the following path:
File/Examples/Update/ AWS_S3_OTA_UPDATE. In this variables, we'll need to 
change the network with ours
```
const char* SSID = "Moncada";
const char* PSWD = "lohibr2000";

```
After that, we need to generate the binary file which we would like to 
update by OTA and uploaded to S3 bucket, it will generate us a link which we will divided
and use it for the String host and String bin variables

Then, divide the link, for String host use from "ats" to ".com" nothing else, and 
for String bin take it from "/esp32" to ".bin" all inside double quotes

```
String host = "ats-iot-jobs.s3.us-west-2.amazonaws.com"; // Host => bucket-name.s3.region.amazonaws.com
String bin = "/esp32/blink.ino.ino.wireless_stick_lite.bin"; // bin file name with a slash in front.
// Link given by S3 example: https:///esp32/blink.ino.ino.wireless_stick_lite.bin

```
## Example Output
```
Connecting to Moncada

Connected to Moncada
Connecting to: ats-iot-jobs.s3.us-west-2.amazonaws.com
Fetching Bin: /esp32/blink.ino.ino.wireless_stick_lite.bin
Got application/octet-stream payload.
Got 207824 bytes from server
contentLength : 207824, isValidContentType : 1
Begin OTA. This may take 2 - 5 mins to complete. Things might be quite for a while.. Patience!
Written : 207824 successfully
OTA done!
Update successfully completed. Rebooting.
ets Jun  8 2016 00:22:57

```
## Troubleshooting
Check the connection for wifi network, availability, and status. Check if the variables are correct and the file 
is uploaded correctly, if doens't uploaded the program won't work as expected
