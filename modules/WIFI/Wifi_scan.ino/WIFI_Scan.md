# WIFi Scan example
This is an example and show how to use the WIFi Scan for ESP32 with Arduino
Using the example provided by Arduino. 

## How to use
Including the library Wifi.h we'll scan all the close available networks for the ESP32,
Then, initialize the board's wifi module and start to scan, shows the network's name and print the RSSI
## Example Output
You will see an output like this and all the networks
```
scan start
scan done
1 networks found
1: Moncada (-71)*

```
## Troubleshooting
If you have issues check if the network is turned on and available, and the baudrate for serial monitor
