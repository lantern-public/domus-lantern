# WIFi connect example
This is an example and show how to use the WIFi connect for ESP32 with Arduino
Using the example provided by Arduino. 

## How to use
Including the library Wifi.h we'll connect to the network we need providing the SSID and the password network
They are saved in a constant variable, just replace for yours.
Then, initialize the board's wifi and check if it's connected, otherwise send a 
message showing that's not connected. 
After that enable the Serial Monitor and check if it's connecting and print the IP
given for the board.

## Example Output
You will see an output like this with a different IP address
```
WiFi connected
IP address: 
192.168.100.133

```
## Troubleshooting
If you have issues check the variables and if the network exists, then
verify the Serial Monitor baudrate.