/*
   Based on Neil Kolban example for IDF: https://github.com/nkolban/esp32-snippets/blob/master/cpp_utils/tests/BLE%20Tests/SampleScan.cpp
   Ported to Arduino ESP32 by Evandro Copercini
*/

#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>

#include <string>

int scanTime = 5; //In seconds
BLEScan* pBLEScan;

class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
    void onResult(BLEAdvertisedDevice advertisedDevice) {
      //Serial.printf("Advertised Device: %s \n", advertisedDevice.toString().c_str());

      // Obtain remote device mac address
      std::string mac_address = advertisedDevice.getAddress().toString();
      Serial.printf("Advertised Device Address: %s \n", mac_address.c_str());

      // Obtain remote device rssi
      Serial.printf("Advertised Device RSSI: %d \n", advertisedDevice.getRSSI());

      // Obtain remote device payload length
      size_t payload_length = advertisedDevice.getPayloadLength();
      Serial.printf("Advertised Device Payload Length: %d \n", payload_length);

      // Obtain remote device payload
      uint8_t payload[payload_length] = "";
      memcpy(payload, advertisedDevice.getPayload(), payload_length);
      Serial.printf("Advertised Device Payload: ");
      for(int i = 0; i < payload_length; i++)
      {
          Serial.printf("%x", payload[i]);
      }
      Serial.printf("\n");

      Serial.printf("\n");
    }
};

void setup() {
  Serial.begin(115200);
  Serial.println("Scanning...");

  BLEDevice::init("");
  pBLEScan = BLEDevice::getScan(); //create new scan
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setActiveScan(true); //active scan uses more power, but get results faster
  pBLEScan->setInterval(100);
  pBLEScan->setWindow(99);  // less or equal setInterval value
}

void loop() {
  // put your main code here, to run repeatedly:
  BLEScanResults foundDevices = pBLEScan->start(scanTime, false);
  Serial.print("Devices found: ");
  Serial.println(foundDevices.getCount());
  Serial.println("Scan done!");
  Serial.println(" ");
  pBLEScan->clearResults();   // delete results fromBLEScan buffer to release memory
  delay(2000);
}
