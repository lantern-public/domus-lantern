
# BLE SCAN EXAMPLE
This code initializes the ESP32 as a BLE device and scans for nearby devices. 
Upload this code to your ESP32. Go to the Serial Monitor with the ESP32 running the “BLE_scan” example

The scanner finds the devices that are close to it with the name, mac address and rssi

If you don't have any beacon from MINEW you could use your smarthphone to get some device for your test.

Download the app NrfConnect from Google Play and enable the Bluetooth of your device.

![Logo](https://i0.wp.com/rntlab.com/wp-content/uploads/2018/02/nRF-connect.jpg?w=828&quality=100&strip=all&ssl=1)
![Logo](https://i0.wp.com/rntlab.com/wp-content/uploads/2018/02/enable-bluetooth-nrf-e1519725728543.jpg?w=828&quality=100&strip=all&ssl=1)
![Logo](https://i0.wp.com/randomnerdtutorials.com/wp-content/uploads/2018/06/ble-server-found.jpg?w=750&quality=100&strip=all&ssl=1)

Click the “Connect” button.

As you can see in the figure below, the ESP32 has a service with the UUID that you’ve defined earlier. If you tap the service, it expands the menu and shows
the Characteristic with the UUID that you’ve also defined.

![Logo](https://i0.wp.com/randomnerdtutorials.com/wp-content/uploads/2018/06/BLE-server-service.jpg?w=450&quality=100&strip=all&ssl=1)

And then open the Serial Monitor and at the moment you will see your phone doing beaconing and your ESP32 will listen it.
```bash
Advertised Device Address: 54:7d:ca:3c:54:c7 
Advertised Device RSSI: -83 
Advertised Device Payload Length: 25 
Advertised Device Payload: 18ff7502140051010042394b6024b50170

Devices found: 1
Scan done!
```
## Troubleshooting
For any kind of issues check the following link to get more information:
https://randomnerdtutorials.com/esp32-bluetooth-low-energy-ble-arduino-ide/