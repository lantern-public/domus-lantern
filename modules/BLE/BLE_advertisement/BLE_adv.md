
# BLE ADVERTISEMENT EXAMPLE
## Code snippet
The libraries used here are

```
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
```
Define service UUID and characteristics UUID.

```
#define SERVICE_UUID "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"
```
All this does is set the “deviceConnected” flag true or false when you connect or disconnect from the ESP32. The callback function that handles receiving data being sent from the client(phone) and Bluetooth connection status.

```
class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};
```
Initialize the ESP32 as a BLE device and set its name.

```
BLEDevice::init("MyESP32");
```
Create BLE server

 ```
 BLEServer *pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());
```
Create BLE service using the service UUID.

BLEService *pService = pServer->createService(SERVICE_UUID);
And add characteristics.PROPERTY_READ, PROPERTY_WRITE, PROPERTY_NOTIFY, PROPERTY_INDICATE are the properties of the characteristics.

```
pCharacteristic = pService->createCharacteristic(
                      CHARACTERISTIC_UUID,
                      BLECharacteristic::PROPERTY_READ   |
                      BLECharacteristic::PROPERTY_WRITE  |
                      BLECharacteristic::PROPERTY_NOTIFY |
                      BLECharacteristic::PROPERTY_INDICATE
                    );
  pCharacteristic->addDescriptor(new BLE2902());
  ```
Start BLE service. Starting a service also means that we can create the corresponding characteristics. Characteristics have various attributes such as UUID, properties, permissions, and values. Properties describe what can be done with characteristics, such as read, write, notify and indicate. Permission describe what value can be allowed eg: read and write.

 ```
  pService->start();
  ```
Start advertising
```
 pServer->getAdvertising()->start();
```
The assigned value of descriptor for client characteristics configuration is 0x2902.

The Client Characteristic Configuration descriptor defines how the characteristic may be configured by a specific client. The client characteristics configuration descriptor is unique for each client. A client may read and write this descriptor to determine and set the configuration for that client. Authentication and authorization may be required by the server to write this descriptor. The default value for the Client Characteristic Configuration descriptor is 0x00.
Sent the notification in every second after the device connected.
```
if (deviceConnected) { 
Serial.printf("*** NOTIFY: %d ***\n", value);
pCharacteristic->setValue(&value, 1); 
pCharacteristic->notify(); 
//pCharacteristic->indicate(); 
value++; 
}
```
![Logo](https://openlabpro.com/wp-content/uploads/2018/10/ble_notify1-500x393.png)
![Logo](https://openlabpro.com/wp-content/uploads/2018/10/BLE_notify2-500x369.png)

## Troubleshooting
For any kind of issues check the following link to get more information:
https://openlabpro.com/guide/ble-notify-on-esp32-controller/