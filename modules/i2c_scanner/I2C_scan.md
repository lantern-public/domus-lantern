
# I2C SCANNER EXAMPLE
For this example, we want to know for an address given from sensor, it will be used for 
write or read an instruction on the sensor.
With I2C communication, each slave on the bus has its own address, a hexadecimal number that allows the ESP32 to communicate with each device.

The I2C address can be usually found on the component’s datasheet. However, if it is difficult to find out, you may need to run an I2C scanner sketch to find out the I2C address.

## How to connect
Connecting an I2C device in this case we will use a BME280 for testing to an ESP32 is normally as simple as connecting GND to GND, SDA to SDA, SCL to SCL and a positive power supply to a peripheral, usually 3.3V

SDA-----> SDA (GPIO 21)

SCL-----> SCL (GPIO 22)

VCC-----> VCC (3.3V)

GND-----> GND 

After that you’ll get something similar in your Serial Monitor. This specific example is for an I2C LCD Display.


![Logo](https://i0.wp.com/randomnerdtutorials.com/wp-content/uploads/2018/07/scan_i2c.png?w=624&quality=100&strip=all&ssl=1)

## Troubleshooting
For any kind of issues check the following link to get more information:
https://randomnerdtutorials.com/esp32-i2c-communication-arduino-ide/