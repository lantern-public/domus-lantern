
# DEEP SLEEP EXAMPLE
This example demonstrate how to use the deep sleep with ESP32 Wireless Stick Lite
in Arduino. If you put your ESP32 in deep sleep mode, it will reduce the power consumption and your batteries will last longer.

Having your ESP32 in deep sleep mode means cutting with the activities that consume more power while operating, but leave just enough activity to wake up the processor when something interesting happens.

In deep sleep mode neither CPU or Wi-Fi activities take place, but the Ultra Low Power (ULP) co-processor can still be powered on.


## How does the code works
You can use different ways to wake up your ESP32 from deep sleep in this case we will use a timer, using the following function to make it works
esp_sleep_enable_timer_wakeup(time_in_us).

These first two lines of code define the period of time the ESP32 will be sleeping.

```bash
#define uS_TO_S_FACTOR 1000000 /* Conversion factor for micro seconds to seconds 
#define TIME_TO_SLEEP 5 /* Time ESP32 will go to sleep (in seconds) 
```
To save data on the RTC memory, you just have to add RTC_DATA_ATTR before a variable definition. The example saves the bootCount variable on the RTC memory. This variable will count how many times the ESP32 has woken up from deep sleep.

```bash
RTC_DATA_ATTR int bootCount = 0;
```
Then, the code defines the print_wakeup_reason() function, that prints the reason by which the ESP32 has been awaken from sleep.

```bash
void print_wakeup_reason(){
  esp_sleep_wakeup_cause_t wakeup_reason;

  wakeup_reason = esp_sleep_get_wakeup_cause();

  switch(wakeup_reason){
    case ESP_SLEEP_WAKEUP_EXT0 : Serial.println("Wakeup caused by external signal using RTC_IO"); break;
    case ESP_SLEEP_WAKEUP_EXT1 : Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
    case ESP_SLEEP_WAKEUP_TIMER : Serial.println("Wakeup caused by timer"); break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD : Serial.println("Wakeup caused by touchpad"); break;
    case ESP_SLEEP_WAKEUP_ULP : Serial.println("Wakeup caused by ULP program"); break;
    default : Serial.printf("Wakeup was not caused by deep sleep: %d\n",wakeup_reason); break;
  }
}
```
This example starts by initializing the serial communication at a baud rate of 115200.

```Serial.begin(115200);```

Then, the bootCount variable is increased by one in every reboot, and that number is printed in the serial monitor.

```bash
++bootCount;
Serial.println("Boot number: " + String(bootCount));
```

Then, the code calls the print_wakeup_reason() function, but you can call any function you want to perform a desired task. For example, you may want to wake up your ESP32 once a day to read a value from a sensor.

Next, the code defines the wake up source by using the following function:

```esp_sleep_enable_timer_wakeup(time_in_us)```

This function accepts as argument the time to sleep in microseconds as we’ve seen previously.

In our case, we have the following:

```esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);```

Then, after all the tasks are performed, the esp goes to sleep by calling the following function:

```esp_deep_sleep_start()```

## Output
Every 5 seconds, the ESP wakes up, prints a message on the serial monitor, and goes to deep sleep again.

Every time the ESP wakes up the bootCount variable increases. It also prints the wake up reason as shown in the figure below.


![Logo](https://i0.wp.com/randomnerdtutorials.com/wp-content/uploads/2019/02/timer-wake-up-serial-monitor.png?w=524&quality=100&strip=all&ssl=1)

## Troubleshooting
For any kind of issues check the following link to get more information:
https://randomnerdtutorials.com/esp32-deep-sleep-arduino-ide-wake-up-sources/