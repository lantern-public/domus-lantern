![Lantern](img/LogoLan-Fondo_Blanco_pequeño.png)
# DOMUS-LANTERN
## What is Domus?
The Domus sensor is a data logger created by LANTERN TECHNOLOGIES with an ESP32 as microprocessor
which has a Bluetooth, WIFI, Ultra-Low-Power, LoRa chip SX1276 integrated, support the 
Arduino development environment. And an incredible feature is you can supply it with continuous current by lithium 
battery or 5v transformer

The main use cases are for agriculture, industry and smarthome. 

## What is a data logger?
A data logger is a device in charge of receiving the pulses coming from the machines
or sensors and convert them into human-readable signals through a process of receiving, filtering, and
interpretation of data, the Domus data logger supports analog and digital pulses and has different 
communication protocols with the receiving device that are then sent to the cloud for integration into 
the platform where the current state of sensor or machine shown

## About LANTERN TECHNOLOGIES
We are a company dedicated to the impulse and development of the Internet of Things 
through the creation of sensors such as data loggers and monitoring of machines at an 
industrial level, in the same way with the agricultural sector and homes, thus obtaining 
relevant data for engineering agricultural and its monitoring as well as its respective alerts in homes
showing them in a personalized platform and apps
## Domus board
The Domus board was designed for different purposes as analog and digital, and you have the 
possibility to connect others sensors to get different measures for example the 
soil moisture, humidity, environment pressure, temperature, rain, wind and distance.

## Acrilic for case

As well as the board and ESP32, the acrilic is relevant to get robustness
and avoid loose parts inside the data logger and easier way to remove the 
microprocessor if required also, the case is waterproof and withstands 
different weather conditions.

## Tested sensors

There are different sensors we tested with ESP32 on Arduino environment:

- BME280 (pressure, temperature, humidity)
- ADS1115
- D6F-PH5050 (differential pressure sensor)
- DS18B20 (soil temperature)

## Tested modules
 
 These are the different modules tested:
 
 - AWS S3 OTA UPDATE
 - BLE (adv, scan)
 - Deep sleep (15uA)
 - HTTP (json post, text post)
 - i2c scanner
 - LoRa (get id, otaa)
 - MQTT (wifi)
 - ULP 
 - WIFI (scan, connect)

 You can see the images attached in img folder.

 ## Contact us
 If you are interested in obtaining any of our 
 products or need help with any of them, do not hesitate to contact us:

 - info@lantern.tech
 - https://www.lantern.tech/

 ![Domus](img/img17.jpeg)